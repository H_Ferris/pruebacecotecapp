/**
 * Test for Cecotec
 * by Héctor Ferrís
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  Button,
  StatusBar,
  Alert
} from 'react-native';

import {
  Colors,
  Metrics
} from '../theme/';

const ProductScreen = ({ navigation, route }) => {
  const { product } = route.params;
  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.productContainer} testID="product_page">
              <Text style={styles.productTitle}>{product.name}</Text>
              <View style={styles.imageContainer}>
                <Image style={styles.productImage}
                  source={{ uri: product.image + "?id=" + product.id.toString() }} />
              </View>
              <View style={styles.sectionText}>
                <Text style={styles.productText}>{product.description}</Text>
              </View>
              <View style={styles.buttonContainer}>
                <Button
                  color={Colors.main}
                  testID="buy_button"
                  title={`BUY ${product.price}`}
                  onPress={() => {
                    Alert.alert(product.name + ' Added to Basket!')
                    navigation.setOptions({
                      headerRight: () => (
                        <View style={styles.headerButton}>
                          <Button color={Colors.white} title="🛒" testID="buy_confirm" />
                        </View>
                      ),
                    });
                  }
                  }
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.mainBackground,
  },
  body: {
    backgroundColor: Colors.mainBackground,
  },
  productContainer: {
    marginTop: Metrics.baseSpace,
    paddingHorizontal: Metrics.plusSpace,
  },
  imageContainer: {
    paddingHorizontal: Metrics.plusSpace,
    flexDirection: "row"
  },
  productTitle: {
    textAlign: "center",
    fontSize: Metrics.fonts.h2,
    fontWeight: 'bold',
    color: Colors.black80,
    marginBottom: Metrics.smallSpace
  },
  productText: {
    flex: 1,
    padding: Metrics.baseSpace,
    fontSize: Metrics.fonts.body2,
    fontWeight: '400',
    color: Colors.gray,
  },
  productImage: {
    flex: 1,
    height: Metrics.images.imageBig,
    width: Metrics.images.imageBig
  },
  buttonContainer: {
    margin: Metrics.baseSpace,
  },
  headerButton: {
    marginRight: Metrics.smallSpace,
  },
});

export default ProductScreen;
