/**
 * Test for Cecotec
 * by Héctor Ferrís
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  FlatList,
  ActivityIndicator
} from 'react-native';
import { observer } from "mobx-react";

import {
  Colors,
  Metrics
} from '../theme/';
import Store from '../mobx/store'
import ProductListRow from "../components/ProductListRow";

const HomeScreen = observer(({ navigation }) => {
  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView>
        <View style={styles.body}>
          <View style={styles.sectionContainer}>
            <Text style={styles.sectionTitle}>List of products</Text>
            <Text style={styles.sectionSubtitle}>
              Our best products.
              </Text>
          </View>
          <View style={styles.listContainer} testID="products">
            <FlatList
              data={Store.products}
              renderItem={({ item }) => <ProductListRow product={item} navigation={navigation} />}
              keyExtractor={item => item.id.toString()}
              ListEmptyComponent={<View style={styles.sectionContainer}><Text style={styles.highlight}>Loading products...</Text></View>}
            />
            <View style={styles.loadingContainer}>{Store.loading && <ActivityIndicator size={Metrics.icons.giant} color={Colors.highlight} />}</View>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
});

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.mainBackground,
  },
  body: {
    backgroundColor: Colors.mainBackground,
  },
  sectionContainer: {
    marginTop: Metrics.plusSpace,
    paddingHorizontal: Metrics.bigSpace,
  },
  listContainer: {
    maxHeight: Metrics.screenHeight - Metrics.giantSpace,
    width: Metrics.screenWidth - Metrics.baseSpace
  },
  loadingContainer: {
    margin: Metrics.plusSpace,
  },
  sectionTitle: {
    fontSize: Metrics.fonts.h1,
    fontWeight: 'bold',
    color: Colors.aqua,
  },
  sectionSubtitle: {
    marginTop: Metrics.miniSpace,
    marginBottom: Metrics.smallSpace,
    fontSize: Metrics.fonts.body1,
    fontWeight: '400',
    color: Colors.main,
    fontStyle: "italic"
  },
  buttonContainer: {
    margin: Metrics.baseSpace,
  },
});

export default HomeScreen;
