/**
 * Test for Cecotec
 * by Héctor Ferrís
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
} from 'react-native';

import {
    Colors,
    Metrics
} from '../theme/';

const ProductScreen = ({ product, navigation }) => {
    return (
        <>
            <View style={styles.body}>
                <TouchableOpacity style={styles.productContainer}
                    onPress={() =>
                        navigation.navigate('Product', { product })
                    }
                    testID="product_button">
                    <Text style={styles.productTitle}>{product.name}</Text>
                    <View style={styles.imageContainer}>
                        <Image
                            style={styles.productImage}
                            source={{ uri: product.image + "?id=" + product.id.toString() }}
                            resizeMode="cover"
                        />
                        <Text style={styles.productText}
                            numberOfLines={5}>{product.description}</Text>
                    </View>
                    <View style={styles.buttonContainer}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>View product</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    body: {
        backgroundColor: Colors.mainBackground,
        paddingLeft: Metrics.baseSpace
    },
    productContainer: {
        backgroundColor: Colors.boxBackground,
        marginTop: Metrics.baseSpace,
        paddingHorizontal: Metrics.baseSpace,
        borderWidth: 0.5,
        borderColor: Colors.lightGray
    },
    imageContainer: {
        paddingHorizontal: Metrics.baseSpace,
        flexDirection: "row"
    },
    productTitle: {
        paddingHorizontal: Metrics.plusSpace,
        marginTop: Metrics.miniSpace,
        marginBottom: Metrics.miniSpace,
        fontSize: Metrics.fonts.h2,
        fontWeight: 'bold',
        color: Colors.black80,
    },
    productText: {
        flex: 0.5,
        padding: Metrics.smallSpace,
        fontSize: Metrics.fonts.body2,
        fontWeight: '400',
        color: Colors.gray,
    },
    productImage: {
        flex: 0.5,
        height: Metrics.images.image,
        width: Metrics.images.image
    },
    buttonContainer: {
        margin: Metrics.baseSpace,
        paddingHorizontal: Metrics.doubleSpace
    },
    button: {
        backgroundColor: Colors.white,
        paddingHorizontal: Metrics.plusSpace,
        marginTop: Metrics.miniSpace,
        marginBottom: Metrics.miniSpace,
        borderWidth: 2,
        borderColor: Colors.main,
    },
    buttonText: {
        paddingHorizontal: Metrics.plusSpace,
        marginTop: Metrics.miniSpace,
        marginBottom: Metrics.miniSpace,
        fontSize: Metrics.fonts.button,
        fontWeight: 'bold',
        color: Colors.main,
        textAlign: "center"
    },
});

export default ProductScreen;
