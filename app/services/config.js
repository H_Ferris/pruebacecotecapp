const env = "dev"; // "dev"; // "local"; // "production"; // <--Cambiar aquí!

console.log("Enviroment >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ", env);

let apiBase;

switch (env) {
    case "dev":
        apiBase = "https://gorest.co.in/public-api/";
        break;

    case "local":
        apiBase = "http://127.0.0.1:3050/";
        break;

    case "production":
        apiBase = "https://whateverurlinproduction.amazonaws.com/prod/";
        break;

    default:
        break;
}
export default {
    env,
    apiBase,
};