import apisauce from "apisauce";

import AppConfig from "./config";

let instance = null;

const create = (baseURL = AppConfig.apiBase) => {
    if (instance !== null) {
        return instance;
    }

    const api = apisauce.create({
        baseURL,
        headers: {
            "Cache-Control": "no-cache",
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        timeout: 30000
    });

    api.setAuthToken = token => {
        if (token === null) {
            delete api.headers.Authorization;
        } else {
            api.setHeader("Authorization", token);
        }
    };

    api.getAllProducts = code => {
        console.log(`Fetching products!`);
        return api.get(`products`);
    };

    instance = api;

    return instance;
};

export default {
    create
};