import { Alert } from "react-native";
import { makeObservable, observable, action } from "mobx";

import API from "../services/api";

const api = API.create();

class Store {

    loading = false;

    products = [];

    constructor() {
        makeObservable(this, {
            products: observable,
            loading: observable,
            toogleLoading: action,
            fillProducts: action
        });
        this.getAllProducts();
    }

    getAllProducts() {
        this.toogleLoading(true);
        api.getAllProducts().then(response => {
            if (
                response.status === 200 &&
                response.data
            ) {
                this.fillProducts(response.data.data);
                this.toogleLoading(false);
                return true;
            }
            if (response && response.status === 400) {
                Alert.alert(
                    "Error",
                    response.originalError
                        ? response.originalError.toString()
                        : response.problem
                );
                this.toogleLoading(false);
                return false;
            }
        });
    }

    toogleLoading(tof) {
        this.loading = tof;
    }

    fillProducts(data) {
        this.products = data;
    }

}

export default new Store();
