/**
 * @format
 */

import 'react-native';
import React from 'react';
import HomeScreen from '../app/containers/HomeScreen';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

jest.mock('../app/mobx/store');

it('renders correctly', async () => {
  renderer.create(<HomeScreen />);
});
