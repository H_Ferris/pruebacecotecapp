/**
 * @format
 */

import 'react-native';
import React from 'react';
import ProductListRow from '../app/components/ProductListRow';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

const data = {
  "id": 3,
  "name": "Ergonomic Granite Bottle",
  "description": "Sono ater coaegresco cenaculum vicinus aeternus coma degusto demoror rerum tabgo est enim terebro auctus accusamus colo torqueo voluptatem cetera avoco aut stella vulpes cupio temptatio thema labore atqui creo corrupti commemoro certe clementia voluptas censura defendo abduco apparatus cultellus arbor vespillo vis absconditus comes ea sulum audax vorax trucido.",
  "image": "https://lorempixel.com/250/250",
  "price": "27310.38",
  "discount_amount": "19421.83",
  "status": true,
  "categories": [
    {
      "id": 3,
      "name": "Jewelry, Tools & Outdoors"
    },
    {
      "id": 8,
      "name": "Music, Garden & Clothing"
    },
    {
      "id": 12,
      "name": "Computers"
    }
  ]
}

it('renders correctly', async () => {
  renderer.create(<ProductListRow product={data} />);
});
