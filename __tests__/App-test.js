/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../App';

// Note: test renderer must be required after react-native.
import { render, waitFor } from '@testing-library/react-native';

test('Load App and wait for Home', async () => {
  const { getByText } = render(<App />);

  await waitFor(() => getByText('Home'));
});