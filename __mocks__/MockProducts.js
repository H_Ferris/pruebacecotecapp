const products = {
    "code": 200,
    "meta": {
        "pagination": {
            "total": 261,
            "pages": 14,
            "page": 1,
            "limit": 20
        }
    },
    "data": [
        {
            "id": 1,
            "name": "Enormous Granite Chair",
            "description": "Denuncio curia barba trucido concedo tremo degenero umerus vilis congregatio aptus stabilis iusto tabernus aurum custodia adsuesco bonus adficio doloribus earum beatae cubicularis strues unde ascit crapula crux vitium omnis laboriosam utor callide stella iure verbum peior vergo expedita et vis verumtamen apto absum pariatur demo sunt acer.",
            "image": "https://lorempixel.com/250/250",
            "price": "23630.68",
            "discount_amount": "3038.81",
            "status": true,
            "categories": [
                {
                    "id": 6,
                    "name": "Computers & Toys"
                },
                {
                    "id": 4,
                    "name": "Books, Kids & Industrial"
                },
                {
                    "id": 8,
                    "name": "Music, Garden & Clothing"
                }
            ]
        },
        {
            "id": 2,
            "name": "Enormous Leather Bag",
            "description": "Creber venia velit sint barba surgo laudantium adicio confero careo adaugeo xiphias via volutabrum aduro centum colo concido nam degusto summa abscido apto voluptate volva turpe pauper tantum odio utroque tametsi.",
            "image": "https://lorempixel.com/250/250",
            "price": "35027.35",
            "discount_amount": "11258.9",
            "status": true,
            "categories": [
                {
                    "id": 3,
                    "name": "Jewelry, Tools & Outdoors"
                },
                {
                    "id": 2,
                    "name": "Games"
                },
                {
                    "id": 7,
                    "name": "Baby & Games"
                },
                {
                    "id": 12,
                    "name": "Computers"
                },
                {
                    "id": 8,
                    "name": "Music, Garden & Clothing"
                }
            ]
        },
        {
            "id": 3,
            "name": "Ergonomic Granite Bottle",
            "description": "Sono ater coaegresco cenaculum vicinus aeternus coma degusto demoror rerum tabgo est enim terebro auctus accusamus colo torqueo voluptatem cetera avoco aut stella vulpes cupio temptatio thema labore atqui creo corrupti commemoro certe clementia voluptas censura defendo abduco apparatus cultellus arbor vespillo vis absconditus comes ea sulum audax vorax trucido.",
            "image": "https://lorempixel.com/250/250",
            "price": "27310.38",
            "discount_amount": "19421.83",
            "status": true,
            "categories": [
                {
                    "id": 3,
                    "name": "Jewelry, Tools & Outdoors"
                },
                {
                    "id": 8,
                    "name": "Music, Garden & Clothing"
                },
                {
                    "id": 12,
                    "name": "Computers"
                }
            ]
        },
        {
            "id": 4,
            "name": "Mediocre Paper Table",
            "description": "Cerno avoco aegre similique trepide appositus repellendus ulciscor subiungo amplus auctor vitae clibanus nam suffragium incidunt conatus surculus super dolorem curvus rerum audentia versus et celebrer absque commodo est viduata vinum aeternus quos.",
            "image": "https://lorempixel.com/250/250",
            "price": "10353.8",
            "discount_amount": "7047.23",
            "status": true,
            "categories": [
                {
                    "id": 3,
                    "name": "Jewelry, Tools & Outdoors"
                },
                {
                    "id": 10,
                    "name": "Electronics, Sports & Movies"
                },
                {
                    "id": 6,
                    "name": "Computers & Toys"
                }
            ]
        },
        {
            "id": 5,
            "name": "Rustic Bronze Shoes",
            "description": "Acquiro tergo qui triumphus cogo alias laborum acies verecundia labore ambulo et ipsum stips uberrime explicabo voluptas absorbeo certus vinculum qui concido somnus cattus supra spiculum ocer conspergo cibo infit viscus armo ventito usitas vigor cultura vel accendo trado brevis sulum curo caveo sursum xiphias umerus curvus vehemens.",
            "image": "https://lorempixel.com/250/250",
            "price": "15339.43",
            "discount_amount": "12611.69",
            "status": true,
            "categories": [
                {
                    "id": 2,
                    "name": "Games"
                },
                {
                    "id": 7,
                    "name": "Baby & Games"
                },
                {
                    "id": 4,
                    "name": "Books, Kids & Industrial"
                },
                {
                    "id": 5,
                    "name": "Sports"
                },
                {
                    "id": 10,
                    "name": "Electronics, Sports & Movies"
                },
                {
                    "id": 12,
                    "name": "Computers"
                }
            ]
        },
        {
            "id": 6,
            "name": "Intelligent Leather Shoes",
            "description": "Perferendis alter ceno aeneus comis at vobis deripio inflammatio volup speculum molestias causa stipes cohors totus depromo absum terebro id sto cernuus accendo soluta ciminatio arceo denuncio atrox aestas consequuntur quia inventore itaque agnosco crinis ullus quas terra maxime curiositas quisquam toties ascit corrumpo nemo sono.",
            "image": "https://lorempixel.com/250/250",
            "price": "22559.59",
            "discount_amount": "10121.58",
            "status": true,
            "categories": [
                {
                    "id": 1,
                    "name": "Sports, Outdoors & Industrial"
                },
                {
                    "id": 5,
                    "name": "Sports"
                },
                {
                    "id": 3,
                    "name": "Jewelry, Tools & Outdoors"
                },
                {
                    "id": 2,
                    "name": "Games"
                }
            ]
        },
        {
            "id": 7,
            "name": "Synergistic Granite Chair",
            "description": "Qui adsidue utor armo cetera coruscus sunt crebro tollo concedo ciminatio conservo ventito alo et tricesimus tero adinventitias cilicium ustulo iusto textilis vinco valde deduco consequuntur accedo coerceo.",
            "image": "https://lorempixel.com/250/250",
            "price": "16923.78",
            "discount_amount": "14252.89",
            "status": true,
            "categories": [
                {
                    "id": 1,
                    "name": "Sports, Outdoors & Industrial"
                },
                {
                    "id": 10,
                    "name": "Electronics, Sports & Movies"
                },
                {
                    "id": 9,
                    "name": "Electronics & Garden"
                },
                {
                    "id": 11,
                    "name": "Kids & Grocery"
                },
                {
                    "id": 3,
                    "name": "Jewelry, Tools & Outdoors"
                },
                {
                    "id": 8,
                    "name": "Music, Garden & Clothing"
                }
            ]
        },
        {
            "id": 8,
            "name": "Fantastic Rubber Chair",
            "description": "Tonsor arx virga dens qui ambitus contra careo tametsi adversus acies voro sit creo teneo tribuo utroque contabesco hic iste iure crudelis quia neque theatrum veniam excepturi tum qui nihil adfero modi commodo delicate.",
            "image": "https://lorempixel.com/250/250",
            "price": "33954.67",
            "discount_amount": "30018.37",
            "status": true,
            "categories": [
                {
                    "id": 3,
                    "name": "Jewelry, Tools & Outdoors"
                },
                {
                    "id": 1,
                    "name": "Sports, Outdoors & Industrial"
                },
                {
                    "id": 9,
                    "name": "Electronics & Garden"
                },
                {
                    "id": 2,
                    "name": "Games"
                },
                {
                    "id": 4,
                    "name": "Books, Kids & Industrial"
                }
            ]
        },
        {
            "id": 9,
            "name": "Fantastic Aluminum Lamp",
            "description": "Utpote teneo aedificium voluptatem quibusdam deficio decipio est amoveo dignissimos coruscus commodo currus quis tolero vorax confido eum adulatio angulus supra possimus cervus stillicidium tantum conqueror arto absens cornu admoveo attollo est amissio deprecator clamo claro ante eligendi corrupti nesciunt vir beneficium consuasor est creta aliqua unde earum accommodo laborum averto supplanto appono vestrum textus sopor vado magni avaritia cultellus sursum aestivus truculenter credo.",
            "image": "https://lorempixel.com/250/250",
            "price": "38210.17",
            "discount_amount": "25115.6",
            "status": true,
            "categories": [
                {
                    "id": 3,
                    "name": "Jewelry, Tools & Outdoors"
                }
            ]
        },
        {
            "id": 10,
            "name": "Mediocre Paper Car",
            "description": "Decipio vinitor tibi cogo non patruus cinis auxilium vomica defigo accipio deleniti deludo tener conscendo claro et vultuosus comptus apto coniuratio laudantium bis vulnero et convoco nobis damnatio adultus atqui ait tenus vulpes tamquam avaritia corroboro nemo voluptas voluptatem contra carcer sui cribro verecundia alo calamitas laborum crepusculum rerum labore patior strues vinum.",
            "image": "https://lorempixel.com/250/250",
            "price": "23672.7",
            "discount_amount": "13734.92",
            "status": true,
            "categories": [
                {
                    "id": 11,
                    "name": "Kids & Grocery"
                },
                {
                    "id": 9,
                    "name": "Electronics & Garden"
                },
                {
                    "id": 4,
                    "name": "Books, Kids & Industrial"
                },
                {
                    "id": 10,
                    "name": "Electronics, Sports & Movies"
                }
            ]
        },
        {
            "id": 11,
            "name": "Heavy Duty Bronze Computer",
            "description": "Sordeo non viduo animi audacia capillus molestias clibanus admoneo compono tepesco cupiditas auris comprehendo carbo arto accusantium cibo quos rem consequuntur ipsa caute amet coruscus volva defaeco vespillo quam adfectus adaugeo undique adsidue benevolentia accusator vivo callide caterva denego somnus qui subiungo amo amplexus nostrum ubi ustilo timor caveo sint concido.",
            "image": "https://lorempixel.com/250/250",
            "price": "44290.64",
            "discount_amount": "19132.48",
            "status": true,
            "categories": [
                {
                    "id": 2,
                    "name": "Games"
                },
                {
                    "id": 11,
                    "name": "Kids & Grocery"
                }
            ]
        },
        {
            "id": 12,
            "name": "Incredible Plastic Bench",
            "description": "Contabesco auxilium reiciendis triduana condico atqui sapiente defungo crudelis et sortitus toties atrox deprecator tamisium placeat animi dapifer varius hic verbera ceno vulgus amplexus crastinus suasoria via annus adimpleo bestia cupressus accusator timor valetudo decimus demum caries totus depulso vicinus umbra dolorum dolorem animus adstringo amplus apparatus quis terror amoveo aeger advenio occaecati vel thalassinus omnis suadeo turbo circumvenio.",
            "image": "https://lorempixel.com/250/250",
            "price": "7048.64",
            "discount_amount": "5322.29",
            "status": true,
            "categories": [
                {
                    "id": 5,
                    "name": "Sports"
                }
            ]
        },
        {
            "id": 13,
            "name": "Durable Iron Plate",
            "description": "Aestus despirmatio usitas accusator quod altus texo consequatur vindico communis cognatus dicta cuppedia vetus tardus ulciscor uberrime distinctio sint centum coadunatio tumultus appositus civis armarium benigne adstringo tabella aro.",
            "image": "https://lorempixel.com/250/250",
            "price": "10561.13",
            "discount_amount": "7338.18",
            "status": true,
            "categories": [
                {
                    "id": 2,
                    "name": "Games"
                },
                {
                    "id": 4,
                    "name": "Books, Kids & Industrial"
                },
                {
                    "id": 3,
                    "name": "Jewelry, Tools & Outdoors"
                },
                {
                    "id": 12,
                    "name": "Computers"
                },
                {
                    "id": 5,
                    "name": "Sports"
                }
            ]
        },
        {
            "id": 14,
            "name": "Small Aluminum Plate",
            "description": "Voluptatum denique via viridis adulatio terra verbum conatus et possimus auctus rerum apparatus tibi vilicus tendo aureus ulterius valens audeo tactus deludo urbanus civis deduco patior alveus corporis arceo maiores comminor molestiae paens velociter synagoga balbus cauda caelum casso explicabo tego statua defendo universe carus tardus vestrum cumque sit cupiditas virtus velit quo candidus sollicito doloremque claro abeo.",
            "image": "https://lorempixel.com/250/250",
            "price": "44488.83",
            "discount_amount": "21084.84",
            "status": true,
            "categories": [
                {
                    "id": 7,
                    "name": "Baby & Games"
                },
                {
                    "id": 8,
                    "name": "Music, Garden & Clothing"
                },
                {
                    "id": 2,
                    "name": "Games"
                },
                {
                    "id": 12,
                    "name": "Computers"
                }
            ]
        },
        {
            "id": 15,
            "name": "Aerodynamic Linen Knife",
            "description": "Cubo ara caelestis tremo id civis ascit congregatio rerum totam summopere varius terreo thema comedo aut vomer decens textor absorbeo et asper decet ultra defluo despecto dapifer consequuntur tepesco.",
            "image": "https://lorempixel.com/250/250",
            "price": "48869.85",
            "discount_amount": "25403.98",
            "status": true,
            "categories": [
                {
                    "id": 1,
                    "name": "Sports, Outdoors & Industrial"
                },
                {
                    "id": 3,
                    "name": "Jewelry, Tools & Outdoors"
                },
                {
                    "id": 11,
                    "name": "Kids & Grocery"
                },
                {
                    "id": 10,
                    "name": "Electronics, Sports & Movies"
                },
                {
                    "id": 5,
                    "name": "Sports"
                },
                {
                    "id": 4,
                    "name": "Books, Kids & Industrial"
                }
            ]
        },
        {
            "id": 16,
            "name": "Enormous Cotton Computer",
            "description": "Pectus ager sit clibanus amitto eum blandior maiores ipsa deprecator exercitationem pecus usque sordeo ambulo certo quia nesciunt qui quia tenetur amiculum deleo tubineus vapulus culpa nobis testimonium subito depopulo ulterius earum suffoco adiuvo vulgivagus vitae aut totam cauda tamquam alias nulla victus aliquid pauper subvenio id varietas corrupti ut cohors nemo reiciendis laboriosam ut accipio adipisci basium amoveo expedita eligendi aegre catena.",
            "image": "https://lorempixel.com/250/250",
            "price": "20816.7",
            "discount_amount": "9997.93",
            "status": true,
            "categories": [
                {
                    "id": 6,
                    "name": "Computers & Toys"
                }
            ]
        },
        {
            "id": 17,
            "name": "Ergonomic Cotton Knife",
            "description": "Cado creo vito alo adsum agnitio appositus cornu apparatus adulescens angulus eaque molestias titulus cruciamentum claro damno desidero amplexus clamo spoliatio aut comburo verecundia aequitas voluptates nihil terra corona at eos quis umquam voluptate thymbra comptus adstringo stella crudelis damnatio architecto subnecto termes demergo cena ciminatio cubitum id beatus sulum subiungo vita delego trado deinde abstergo.",
            "image": "https://lorempixel.com/250/250",
            "price": "20353.69",
            "discount_amount": "12683.72",
            "status": true,
            "categories": [
                {
                    "id": 4,
                    "name": "Books, Kids & Industrial"
                },
                {
                    "id": 7,
                    "name": "Baby & Games"
                },
                {
                    "id": 1,
                    "name": "Sports, Outdoors & Industrial"
                },
                {
                    "id": 6,
                    "name": "Computers & Toys"
                },
                {
                    "id": 2,
                    "name": "Games"
                }
            ]
        },
        {
            "id": 18,
            "name": "Ergonomic Linen Lamp",
            "description": "Suppono dicta soluta trado utrum careo texo autem depromo deorsum velut amissio absconditus crepusculum vivo ademptio culpa suggero et surgo cogo pariatur thorax confugo vehemens voluptatum curso sol rerum tabgo cunctatio conqueror valeo similique repellendus umquam certo animi solutio accedo strenuus decumbo caritas saepe vir.",
            "image": "https://lorempixel.com/250/250",
            "price": "21499.26",
            "discount_amount": "1790.15",
            "status": true,
            "categories": [
                {
                    "id": 3,
                    "name": "Jewelry, Tools & Outdoors"
                },
                {
                    "id": 9,
                    "name": "Electronics & Garden"
                }
            ]
        },
        {
            "id": 19,
            "name": "Practical Wooden Shoes",
            "description": "Voluptate vacuus causa vitium aperte calco unde atrocitas adduco et sodalitas vester benevolentia iste caritas viscus vae coma sollicito quis aqua assentator deripio optio depulso canis sophismata sumptus ipsa calcar est dicta asper nisi dolorem aurum conicio demens spoliatio abstergo tabella atqui creta spargo truculenter angustus vis substantia considero utpote ago auditor volva alias sufficio sopor desidero patruus claro animi suffragium viriliter.",
            "image": "https://lorempixel.com/250/250",
            "price": "5054.91",
            "discount_amount": "57.73",
            "status": true,
            "categories": [
                {
                    "id": 4,
                    "name": "Books, Kids & Industrial"
                },
                {
                    "id": 8,
                    "name": "Music, Garden & Clothing"
                },
                {
                    "id": 2,
                    "name": "Games"
                },
                {
                    "id": 1,
                    "name": "Sports, Outdoors & Industrial"
                },
                {
                    "id": 12,
                    "name": "Computers"
                }
            ]
        },
        {
            "id": 20,
            "name": "Intelligent Steel Plate",
            "description": "Carcer et beneficium coniecto delicate adeo adduco turpis venustas armarium esse vae tres tredecim deporto candidus dolor color cognatus alveus alo doloremque admoveo advoco argentum teneo qui degenero arcus uxor summisse.",
            "image": "https://lorempixel.com/250/250",
            "price": "10356.76",
            "discount_amount": "3621.89",
            "status": true,
            "categories": [
                {
                    "id": 10,
                    "name": "Electronics, Sports & Movies"
                },
                {
                    "id": 7,
                    "name": "Baby & Games"
                },
                {
                    "id": 9,
                    "name": "Electronics & Garden"
                },
                {
                    "id": 12,
                    "name": "Computers"
                },
                {
                    "id": 8,
                    "name": "Music, Garden & Clothing"
                }
            ]
        }
    ]
}