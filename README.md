# Install

Type in the ~pruebacecotecapp>
```
yarn
```
 for install packages

 for ios you need also run 
 ```
yarn pods
```

# Run

Type in the ~pruebacecotecapp> 
```
yarn android
```
 for run in Android emulator or connected device

 ```
yarn ios
```
 for run in iOS emulator, for run in a connected device you'll need XCode and probably some aditional config.

 # Install in Device

Type in the ~pruebacecotecapp> 
```
yarn phone
```
 for run in Android phone and work without being wired to PC


 # Unit Testing

Type in the ~pruebacecotecapp> 
```
yarn test
```
For run unit tests in console

# Functional end to end Testing with Detox on Android

Install Detox Cli
```
npm install -g detox-cli
```

Type in the ~pruebacecotecapp> 
```
yarn test-android
```
For run functional tests in emulator

 **To run it again you should close the emulator and let it start again**

*Thats a little bit more tricky as you nedd **a corrrect version of Android Emulator**

 ## Adapt emulator to test Detox

 Follow the https://github.com/wix/Detox/blob/master/docs/Introduction.AndroidDevEnv.md guide to set Pixel_API_28 emulator.

# Functional end to end Testing with Detox on iOS

Type in the ~pruebacecotecapp> 
```
yarn test-ios
```
For run functional tests in iOS emulator

You probably need install simutils
```
brew tap wix/brew
brew install applesimutils
```