describe('Test App', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should show Products screen and load products', async () => {
    await expect(element(by.id('products'))).toBeVisible();
    await expect(element(by.id('product_button')).atIndex(1)).toBeVisible();
  });

  it('should show product screen after tap and return back', async () => {
    await element(by.id('product_button')).atIndex(1).tap();
    await expect(element(by.id('product_page'))).toBeVisible();
    await element(by.id("header-back")).tap();
  });

  it('should show Buy Confirmation screen after tap Buy button', async () => {
    await expect(element(by.id('products'))).toBeVisible();
    await element(by.id('product_button')).atIndex(1).tap();
    await expect(element(by.id('product_page'))).toBeVisible();
    await element(by.id('buy_button')).tap();
    await element(by.text('OK')).tap();
    await expect(element(by.id('buy_confirm'))).toBeVisible();
  });
});
